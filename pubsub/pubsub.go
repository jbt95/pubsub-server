package pubsub

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

type Connection struct {
	issuerId string
	ws       *websocket.Conn
}

type PubSubMsg struct {
	Action   string      `json:"action"`
	Topic    string      `json:"topic"`
	Msg      interface{} `json:"msg"`
	IssuerId string      `json:"issuerId"`
	conn     *Connection
}

type PubSubServer struct {
	unsubscribeC chan PubSubMsg
	subscribeC   chan PubSubMsg
	publishC     chan PubSubMsg
	pingWait     time.Duration
	ticker       *time.Ticker
	conns        map[string][]*Connection
	mtx          sync.Mutex
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  2048,
	WriteBufferSize: 2048,
}

func New() *PubSubServer {
	p := &PubSubServer{
		publishC:     make(chan PubSubMsg),
		unsubscribeC: make(chan PubSubMsg),
		subscribeC:   make(chan PubSubMsg),
		conns:        make(map[string][]*Connection, 0),
		ticker:       time.NewTicker(time.Second),
		pingWait:     time.Second,
	}
	go p.listen()
	return p
}

func (p *PubSubServer) listen() {
	for {
		select {
		case msg := <-p.publishC:
			go p.publish(msg)
		case msg := <-p.subscribeC:
			go p.subscribe(msg)
		case msg := <-p.unsubscribeC:
			go p.unsubscribe(msg)
		case <-p.ticker.C:
			p.ping()
		}
	}
}

func (p *PubSubServer) ping() {
	for _, conns := range p.conns {
		for i, conn := range conns {
			if err := conn.ws.SetWriteDeadline(time.Now().Add(p.pingWait)); err != nil {
				panic(err)
			}
			if err := conn.ws.WriteMessage(websocket.PingMessage, []byte("PING")); err != nil {
				if err := conn.ws.Close(); err != nil {
					panic(err)
				}
				log.Println("connection closed")
				conns = append(conns[:i], conns[i+1:]...)
			}
		}
	}
}

func (p *PubSubServer) handleConnMsg(conn *websocket.Conn) {
	var msg PubSubMsg
	for {
		if err := conn.ReadJSON(&msg); err != nil {
			log.Println(err)
			return
		}
		digest := sha256.Sum256([]byte(msg.Topic))
		msg.Topic = hex.EncodeToString(digest[:])
		msg.conn = &Connection{
			issuerId: msg.IssuerId,
			ws:       conn,
		}
		switch msg.Action {
		case "PUBLISH":
			p.publishC <- msg
			break
		case "SUBSCRIBE":
			p.subscribeC <- msg
			break
		case "UNSUBSCRIBE":
			p.unsubscribeC <- msg
			break
		default:
			break
		}
	}

}

func (p *PubSubServer) publish(msg PubSubMsg) {
	p.mtx.Lock()
	for _, conn := range p.conns[msg.Topic] {
		if err := conn.ws.WriteJSON(&msg); err != nil {
			log.Println(err)
			conn.ws.Close()
		}
	}
	p.mtx.Unlock()
}

func (p *PubSubServer) subscribe(msg PubSubMsg) {
	p.mtx.Lock()
	if _, ok := p.conns[msg.Topic]; !ok {
		p.conns[msg.Topic] = make([]*Connection, 0)
		p.conns[msg.Topic] = append(p.conns[msg.Topic], msg.conn)
		return
	}
	p.conns[msg.Topic] = append(p.conns[msg.Topic], msg.conn)
	p.mtx.Unlock()
}

func (p *PubSubServer) unsubscribe(msg PubSubMsg) {
	p.mtx.Lock()
	for i, conn := range p.conns[msg.Topic] {
		if msg.IssuerId == conn.issuerId {
			p.conns[msg.Topic] = append(p.conns[msg.Topic][:i], p.conns[msg.Topic][i+1:]...)
			err := conn.ws.WriteJSON(&PubSubMsg{
				Action: "UNSUBSCRIBE",
				Topic:  msg.Topic,
				Msg:    fmt.Sprintf("unsubscribed from %v", msg.Topic),
			})
			if err != nil {
				log.Println(err)
				return
			}
		}
	}
	p.mtx.Unlock()
}

func (p *PubSubServer) HandleNewConnection(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer r.Body.Close()
	go p.handleConnMsg(conn)
}
