package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jbt95/pubsub-server/pubsub"
)

func main() {
	p := pubsub.New()
	r := mux.NewRouter()
	log.Println("Listening to port 5000")
	r.HandleFunc("/", p.HandleNewConnection)
	log.Fatal(http.ListenAndServe(":5000", r))
}
